Apollo Read Me

Installation:
    Copy all contents of this folder to a safe, memorable location (i.e. "C:/Program Files/Apollo").
    You will need to associate the executable with supported file types, this can be done manually as you would with any other executable.

Supported Filetypes:
    jpg
    gif
    png

Controls:
    Left/Right Arrows: cycle pictures left and right
    Escape: exit
    D: decorate/undecorate window
    S: toggle slideshow
    C: center
    F1: in-program control dialogue