package org;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static org.imgscalr.Scalr.*;

/**
 * Created by Nick on 7/14/2015.
 * Handle necessary image functions.
 */
public class Image extends JLabel {
    BufferedImage originalImage;
    BufferedImage currImage;
    ImageIcon displayImage;
    public String imagePath;
    int height;
    int width;
    Dimension screenDimensions;
    double aspectRatio;
    public boolean isAnimated;

    public Image(String path){
        screenDimensions = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        imagePath = path;
        // make and
        makeImage();
        update();
    }

    public void makeImage(){
        String extension;
        int i = imagePath.lastIndexOf('.');
        if (i > 0) {
            extension = imagePath.substring(i+1);
        }else extension = "";
        if (extension.equalsIgnoreCase("gif")) {
            displayImage = new ImageIcon(imagePath);
            isAnimated = true;
        }else isAnimated = false;

        try {
            currImage = ImageIO.read(new File(imagePath));

        } catch (IOException e) {
            e.printStackTrace();
        }

        originalImage = currImage;
        height = currImage.getHeight();
        width = currImage.getWidth();
        aspectRatio = ((double) width)/((double)height);
        // aspect ratio unnecessary with animations since no size changes will be made.
        if (!extension.equalsIgnoreCase("gif")) {
            scaleImageToDefault();
        }

        update();
    }

    private void scaleImageToDefault(){
        // if the screen is too small, prescribe the height and width to half of original
        while (width > screenDimensions.width || height > screenDimensions.height){
            width = (int) Math.floor(width/2);
            height = (int) Math.floor(height/2);
        }
        // implement prescription. Method at quality until I can optimize to work with ULTRA_QUALITY
        currImage = org.imgscalr.Scalr.resize(currImage, Method.ULTRA_QUALITY, width, height, OP_ANTIALIAS, OP_BRIGHTER);
    }

    public void scaleImage(int newWidth, int newHeight){
        // check if E/W delta > N/S delta
        if (Math.abs(newWidth - width) > Math.abs(newHeight - height)){
            // E/W is dominant, scale height accordingly
            width = newWidth;
            height = (int) (newWidth/aspectRatio);
        }else {
            // N/S is dominant, scale width accordingly
            height = newHeight;
            width = (int) (newHeight*aspectRatio);
        }
        // set image to scaled image and deploy to frame.
        currImage = org.imgscalr.Scalr.resize(originalImage, Method.SPEED, width, height, OP_ANTIALIAS, OP_BRIGHTER);
        update();
    }

    // adds displayImage to the component
    public void update(){
        if (!isAnimated) displayImage = new ImageIcon(currImage);
        this.setIcon(displayImage);
    }
}
