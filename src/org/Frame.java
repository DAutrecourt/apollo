package org;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Nick on 7/14/2015.
 * build frame and display images.
 */
public class Frame extends JFrame{
    Image currImage;
    Main parent;
    JPanel panel;
    ComponentResizer cr;
    Dimension screenDimensions;
    private Boolean slideshowing;

    public Frame(Image image, Main parent){
        currImage = image;
        this.parent = parent;

        screenDimensions = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        slideshowing = false;

        panel = new JPanel(new GridBagLayout());
        panel.add(currImage);
        this.setContentPane(panel);
        getContentPane().setBackground(Color.black);

        // initialize our resizer component.
        cr = new ComponentResizer();
        cr.currImage = currImage;
        cr.registerComponent(this);
        cr.setMinimumSize(new Dimension(300, 300));

        // basics of JFrame
        this.setIconImage(new ImageIcon(this.getClass().getResource("favicon.png")).getImage());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setSize(currImage.width, currImage.height);
        this.revalidate();
        this.setVisible(true);
        this.InitializeControls();
        this.Update();
    }

    private void InitializeControls() {
        // set inputs
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("LEFT"), "cycleLeft");
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("RIGHT"), "cycleRight");
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "exit");
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("D"), "decorate toggle");
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("S"), "slideshow toggle");
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("C"), "center");
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F1"), "help");

        // set actions for inputs
        panel.getActionMap().put("cycleLeft", new CycleAction(-1));
        panel.getActionMap().put("cycleRight", new CycleAction(1));
        panel.getActionMap().put("exit", new ExitAction());
        panel.getActionMap().put("decorate toggle", new OptionAction("(un)decorate"));
        panel.getActionMap().put("slideshow toggle", new OptionAction("slideshow"));
        panel.getActionMap().put("center", new OptionAction("center"));
        panel.getActionMap().put("help", new OptionAction("help"));
    }

    void Update(){
        // set the title of the frame to the filename currently in view
        Path currentPath = Paths.get(currImage.imagePath);
        String filename = currentPath.getFileName().toString();
        this.setTitle("Apollo - " + filename);
    }

    public void newImage(Image image){
        panel.remove(currImage);
        currImage = image;
        cr.currImage = image;
        panel.add(currImage);
        this.setSize(currImage.width, currImage.height);
        this.revalidate();
        this.Update();
        // if we were in a slideshow, start the slideshow again as it was stopped when a new image was added.
        if (slideshowing){
            slideshowing = false;
            toggleSlideshow();
        }
    }

    private void help() {
        JOptionPane.showMessageDialog(this,
                "Controls:\n" +
                        "Left/Right Arrows: cycle pictures left and right\n" +
                        "Escape: exit\n" +
                        "D: decorate/undecorate window\n" +
                        "S: toggle slideshow\n" +
                        "C: center\n\n" +
                        "Written by N. Taber, 2015",
                "Help",
                JOptionPane.PLAIN_MESSAGE);
    }

    private void toggleSlideshow(){
        if (!slideshowing){
            this.setExtendedState(Frame.MAXIMIZED_BOTH);
            slideshowing = true;
        }else{
            this.setExtendedState(Frame.NORMAL);
            slideshowing = false;
        }
    }

    private void setCenter() {
        this.setLocationRelativeTo(null);
    }

    private void toggleDecoration(){
        if (slideshowing) {
            toggleSlideshow();
            this.dispose();
            this.setVisible(false);
            if (this.isUndecorated()) this.setUndecorated(false);
            else this.setUndecorated(true);
            this.setVisible(true);
            toggleSlideshow();
        }else{
            this.dispose();
            this.setVisible(false);
            if (this.isUndecorated()) this.setUndecorated(false);
            else this.setUndecorated(true);
            this.setVisible(true);
        }
    }

    // cycle between different files.
    private class CycleAction extends AbstractAction {
        int step;
        CycleAction(int step) {
            this.step = step;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            parent.cycleImage(step);
        }
    }

    // toggles and options.
    private class OptionAction extends AbstractAction{
        String setting;
        OptionAction(String set){
            setting = set;
        }
        @Override
        public void actionPerformed(ActionEvent e){
            if (setting.equals("(un)decorate")) toggleDecoration();
            if (setting.equals("slideshow")) toggleSlideshow();
            if (setting.equals("center")) setCenter();
            if (setting.equals("help")) help();
        }
    }

    // simple exit.
    private class ExitAction extends AbstractAction{
        @Override
        public void actionPerformed(ActionEvent e) {
            parent.exit(0);
        }
    }
}
