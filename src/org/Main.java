package org;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Nick on 7/14/2015.
 * Handle files and dispatch to frame.
 */
public class Main {

    Image currImage;
    String currImagePath;
    int currImageIndex;
    Frame frame;

    ArrayList<String> fullDirectory;
    int fullDirectorySize;

    public static void main(String[] args) {
        if (args.length == 0) System.exit(1);
        new Main(args[0]);
    }


    public Main(String path){
        currImage = new Image(path);
        currImagePath = path;
        fullDirectory = new ArrayList<String>();
        getDirectoryFromPathString(path);
        fullDirectorySize = fullDirectory.size();
        currImageIndex = fullDirectory.indexOf(currImagePath);

        frame = new Frame(currImage, this);
    }

    public void cycleImage(int step){
        // apply step
        currImageIndex += step;

        // check out of bounds
        if (currImageIndex > fullDirectorySize - 1) currImageIndex = 0;
        else if (currImageIndex < 0) currImageIndex = fullDirectorySize-1;

        // validate image extension; if validation fails rerun cycle
        String extension;
        int i = fullDirectory.get(currImageIndex).lastIndexOf('.');
        if (i > 0) {
            extension = fullDirectory.get(currImageIndex).substring(i+1);
        }else extension = "";
        if (!extension.equalsIgnoreCase("jpg") &&
                !extension.equalsIgnoreCase("png") &&
                !extension.equalsIgnoreCase("gif")){
            cycleImage(step);
        }

        // set current image and path, dispatch new image creation to frame
        currImagePath = fullDirectory.get(currImageIndex);
        currImage = new Image(currImagePath);
        frame.newImage(currImage);
    }

    void getDirectoryFromPathString(String path){
        String[] splitPath = path.split("\\\\");
        String parentDir = "";
        for (int i = 0; i<splitPath.length - 1; i++){
            parentDir = parentDir + splitPath[i] + "\\";
        }
        File[] allFiles = new File(parentDir).listFiles();
        if (allFiles == null) return;
        for (File f : allFiles){
            if (f.isDirectory()) continue;
            fullDirectory.add(f.toString());
        }
        /*debug("Full Directory: ");
        for (String s: fullDirectory){
            debug("                " + s);
        }*/
    }

    void exit(int Code){
        System.exit(Code);
    }

    /*void debug(String s){
        System.out.println("Apollo | DEBUG: " + s);
    }*/
}
